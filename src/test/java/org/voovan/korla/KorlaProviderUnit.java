package org.voovan.korla;

import org.junit.Test;
import org.voovan.korla.message.Callback;
import org.voovan.korla.message.Msg;
import org.voovan.korla.socket.KorlaProvider;
import org.voovan.tools.TEnv;
import org.voovan.tools.serialize.TSerialize;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 类文字命名
 *
 * @author: helyho
 * korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
public class KorlaProviderUnit {
    static {
        TSerialize.register(TimeMsg.class);
    }

    @Test
    public void test() throws IOException {
        AtomicInteger mm = new AtomicInteger(1);

        KorlaProvider korlaProvider = new KorlaProvider("127.0.0.1", 3333, 500000, 500000);
        korlaProvider.addCallback(new Callback() {
            @Override
            public Msg apply(Msg msg) {
                System.out.println(((TimeMsg) msg).toString());

                TimeMsg timeMsg = (TimeMsg) msg;
                TimeMsg respTimeMsg = (TimeMsg) timeMsg.buildAnswer();
                respTimeMsg.setId(timeMsg.getId());
                respTimeMsg.setSeq(timeMsg.getSeq());
                respTimeMsg.setTime(System.currentTimeMillis());
                return respTimeMsg;
            }
        });

        korlaProvider.listen();

        int m = 0;
        while (m<10000000){
            TEnv.sleep(1);
            m++;
            if(m%1000 == 0) {
                System.out.print("Cache size: " +KorlaStatic.MESSAGE.size());
                System.out.println(", idempotence size: " +KorlaStatic.MESSAGE_IDEMPOTENCE.size());
            }
        }
    }
}
