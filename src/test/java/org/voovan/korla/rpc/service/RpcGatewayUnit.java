package org.voovan.korla.rpc.service;

import org.junit.Test;
import org.voovan.korla.rpc.consumer.RpcMethod;
import org.voovan.korla.rpc.exception.RpcCallbackException;
import org.voovan.korla.rpc.service.selector.LoopSelector;
import org.voovan.korla.rpc.service.selector.RandomSelector;
import org.voovan.tools.TEnv;
import org.voovan.tools.json.JSON;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.function.BiFunction;

/**
 * 类文字命名
 *
 * @author: helyho* korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
public class RpcGatewayUnit {

    @Test
    public void testMain() throws IOException, RpcCallbackException, TimeoutException {

        RpcMethod<String> rpcMethod = new RpcMethod<String>();

        rpcMethod.addConsumer("node-1", "127.0.0.1", 10331);
        rpcMethod.addConsumer("node-2", "127.0.0.1", 10331);



        Object ret1 = rpcMethod.node("node-1").rpc2(123123, "node-1");
        System.out.println(JSON.toJSON(ret1));
        Object ret2 = rpcMethod.node("node-2").rpc2(456456, "node-2");
        System.out.println(JSON.toJSON(ret2));
    }



    @Test
    public void testCustomMain() throws IOException, RpcCallbackException, TimeoutException {

        RpcMethod<Integer> rpcMethod = new RpcMethod<Integer>();
        rpcMethod.selector((k,gateway)->"node-"+k);

        rpcMethod.addConsumer("node-1", "127.0.0.1", 10331);
        rpcMethod.addConsumer("node-2", "127.0.0.1", 10331);


        org.voovan.korla.rpc.TestObject2 testObject2 = new org.voovan.korla.rpc.TestObject2();
        testObject2.setString("rpc4 test");

        Object ret1 = rpcMethod.node(1).rpc2(123123, "node-1");
        System.out.println(JSON.toJSON(ret1));
        Object ret2 = rpcMethod.node(2).rpc2(456456, "node-2");
        System.out.println(JSON.toJSON(ret2));
    }

    @Test
    public void testAnnParamNode() throws IOException, RpcCallbackException, TimeoutException {

        RpcMethod<String>rpcMethod = new RpcMethod<String>();
        rpcMethod.selector((k,gateway)->{
            System.out.println(k);
            return k;
        });

        rpcMethod.addConsumer("node-1", "127.0.0.1", 10331);
        rpcMethod.addConsumer("node-2", "127.0.0.1", 10331);


        org.voovan.korla.rpc.TestObject2 testObject2 = new org.voovan.korla.rpc.TestObject2();
        testObject2.setString("rpc4 test");

        Object ret1 = rpcMethod.rpc4(testObject2, "node-1");
        System.out.println(JSON.toJSON(ret1));
        Object ret2 = rpcMethod.rpc4(testObject2, "node-1");
        System.out.println(JSON.toJSON(ret2));

        Object ret3 = rpcMethod.rpc4(testObject2, "node-2");
        System.out.println(JSON.toJSON(ret3));
        Object ret4 = rpcMethod.rpc4(testObject2, "node-2");
        System.out.println(JSON.toJSON(ret4));
    }

    @Test
    public void testAnnMethodMain() throws IOException, RpcCallbackException, TimeoutException {

        RpcMethod<String> rpcMethod = new RpcMethod<String>();

        rpcMethod.addConsumer("node-1", "127.0.0.1", 10331);
        rpcMethod.addConsumer("node-2", "127.0.0.1", 10331);


        org.voovan.korla.rpc.TestObject2 testObject2 = new org.voovan.korla.rpc.TestObject2();
        testObject2.setString("rpc4 test");

        Object ret1 = rpcMethod.rpc1("node-1", "node-1", 123123);
        System.out.println(JSON.toJSON(ret1));
        Object ret2 = rpcMethod.rpc1("node-2", "node-2", 456456);
        System.out.println(JSON.toJSON(ret2));
    }

    @Test
    public void testRandomMain() throws IOException, RpcCallbackException, TimeoutException {

        RpcMethod<Integer> rpcMethod = new RpcMethod<Integer>();

        rpcMethod.selector(new RandomSelector<>());

        rpcMethod.addConsumer("node-1", "127.0.0.1", 10331);
        rpcMethod.addConsumer("node-2", "127.0.0.1", 10331);
        rpcMethod.addConsumer("node-3", "127.0.0.1", 10331);
        rpcMethod.addConsumer("node-4", "127.0.0.1", 10331);

        org.voovan.korla.rpc.TestObject2 testObject2 = new org.voovan.korla.rpc.TestObject2();
        testObject2.setString("rpc4 test");

        for(int i=0;i<10;i++) {
            Object ret1 = rpcMethod.node(1).rpc2(i, "var=" + i);
            System.out.println(JSON.toJSON(ret1));
            TEnv.sleep(1000);
        }

    }


    @Test
    public void testLoopMain() throws IOException, RpcCallbackException, TimeoutException {

        RpcMethod<Integer> rpcMethod = new RpcMethod<Integer>(5000, 5000, 1, 1);

        rpcMethod.selector(new LoopSelector<Integer>());

        rpcMethod.addConsumer("127.0.0.1", 10331);
        rpcMethod.addConsumer("127.0.0.1", 10331);
        rpcMethod.addConsumer("127.0.0.1", 10331);
        rpcMethod.addConsumer("127.0.0.1", 10331);

        org.voovan.korla.rpc.TestObject2 testObject2 = new org.voovan.korla.rpc.TestObject2();
        testObject2.setString("rpc4 test");

        for(int i=0;i<20;i++) {
            Object ret1 = rpcMethod.rpc2(i, "var=" + i);
            System.out.println(JSON.toJSON(ret1));
            TEnv.sleep(200);
        }

    }
}
