package org.voovan.korla.rpc.service;

import org.junit.Test;
import org.voovan.korla.KorlaStatic;
import org.voovan.korla.rpc.RpcStatic;
import org.voovan.korla.rpc.annotation.Rpc;
import org.voovan.tools.TEnv;

import java.io.IOException;

/**
 * 类文字命名
 *
 * @author: helyho* korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
public class RpcProviderUnit {
    @Test
    public void testMain() throws IOException {
        RpcStatic.RPC_SCAN_PACKAGE = "org.voovan.korla.rpc";

        RpcProvider provider = RpcProvider.newInstance("127.0.0.1", 10331, 5000, 5000);
        provider.setRpcRetry(new RpcRetry() {
            @Override
            public boolean canRetry(int retryCount, Exception e) {
                if(retryCount > 3) {
                    return false;
                }

                TEnv.sleep(5);
                System.out.println("retry......" + retryCount);
                return true;
            }
        });

        //设置分片算法
        provider.getKorlaProvider().setSliceAlgorithm(key->{
            if(key == null) {
                return null;
            }

            long sliceKey = (long)key.hashCode();

            return (int)(sliceKey%31);
        });

        provider.serve();

        int m = 0;
        while (m<10000000){
            TEnv.sleep(1);
            m++;
            if(m%1000 == 0) {
                System.out.print("Cache size: " + KorlaStatic.MESSAGE.size());
                System.out.println(", idempotence size: " +KorlaStatic.MESSAGE_IDEMPOTENCE.size());
            }
        }
    }
}
