package org.voovan.korla.rpc.consumer;

import org.voovan.tools.json.JSON;
import org.voovan.tools.serialize.TSerialize;
import org.voovan.korla.rpc.exception.RpcCallbackException;
import org.voovan.korla.rpc.message.RpcCall;
import org.voovan.korla.rpc.message.RpcCallback;
import org.voovan.korla.rpc.service.RpcConsumer;
import org.voovan.korla.rpc.service.RpcService;

import java.util.*;
import java.math.*;
import java.util.concurrent.TimeoutException;

import org.voovan.korla.rpc.RpcMethod;

public class RpcCommon<K> extends RpcService<RpcCommon, K> {

    public RpcCommon(Integer readTimeout, Integer sendTimeout, Integer minPoolSize, Integer maxPoolSize) {
        super(readTimeout, sendTimeout, minPoolSize, maxPoolSize);

        
    }

    public RpcCommon(String host, int port, int readTimeout, int sendTimeout, int minPoolSize, int maxPoolSize) {
       super(host, port, readTimeout, sendTimeout, minPoolSize, maxPoolSize);

       
    }

    public RpcCommon(String host, Integer port) {
       super(host, port);

       
    }

    public RpcCommon(RpcConsumer consumer) {
       super(consumer);

       
    }

    public RpcCommon() {
        super();

        
    }

    public Collection<RpcMethod> getAllRpcMethod() throws TimeoutException, RpcCallbackException {String node = selector().apply(selectorNode.get(), this);RpcCall rpcCall = RpcCall.newInstance("RpcCommon/getAllRpcMethod");rpcCall.setNode(node);return getByName(node).call(rpcCall);}public void getAllRpcMethod( RpcCallback<Collection<RpcMethod>>callback) {String node = selector().apply(selectorNode.get(), this);RpcCall rpcCall = RpcCall.newInstance("RpcCommon/getAllRpcMethod");rpcCall.setNode(node);getByName(node).call(rpcCall, callback);}
}