package org.voovan.korla.rpc.consumer;

import org.voovan.korla.rpc.TestObject2;
import org.voovan.korla.rpc.exception.RpcCallbackException;
import org.voovan.korla.rpc.message.RpcCall;
import org.voovan.korla.rpc.message.RpcCallback;
import org.voovan.korla.rpc.service.RpcConsumer;
import org.voovan.korla.rpc.service.RpcService;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class RpcMethod<K> extends RpcService<RpcMethod, K> {

    public RpcMethod(Integer readTimeout, Integer sendTimeout, Integer minPoolSize, Integer maxPoolSize) {
        super(readTimeout, sendTimeout, minPoolSize, maxPoolSize);


    }

    public RpcMethod(String host, int port, int readTimeout, int sendTimeout, int minPoolSize, int maxPoolSize) {
        super(host, port, readTimeout, sendTimeout, minPoolSize, maxPoolSize);


    }

    public RpcMethod(String host, Integer port) {
        super(host, port);


    }

    public RpcMethod(RpcConsumer consumer) {
        super(consumer);


    }

    public RpcMethod() {
        super();


    }

    public String rpc1(String var0, String var1, Integer var2) throws TimeoutException, RpcCallbackException {
        String node = selector().apply((K) var0, this);
        RpcCall rpcCall = RpcCall.newInstance("RpcMethod/rpc1", var1, var2);
        rpcCall.setSliceKey(var0);
        rpcCall.setNode(node);
        return getByName(node).call(rpcCall);
    }

    public void rpc1(String var0, String var1, Integer var2, RpcCallback<String> callback) {
        String node = selector().apply((K) var0, this);
        RpcCall rpcCall = RpcCall.newInstance("RpcMethod/rpc1", var1, var2);
        rpcCall.setSliceKey(var0);
        rpcCall.setNode(node);
        getByName(node).call(rpcCall, callback);
    }

    public String rpc2(Integer var0, String var1) throws TimeoutException, RpcCallbackException {
        String node = selector().apply(selectorNode.get(), this);
        RpcCall rpcCall = RpcCall.newInstance("RpcMethod/rpc2", var0, var1);
        rpcCall.setNode(node);
        return getByName(node).call(rpcCall);
    }

    public void rpc2(Integer var0, String var1, RpcCallback<String> callback) {
        String node = selector().apply(selectorNode.get(), this);
        RpcCall rpcCall = RpcCall.newInstance("RpcMethod/rpc2", var0, var1);
        rpcCall.setNode(node);
        getByName(node).call(rpcCall, callback);
    }

    public void rpc3(Integer var0, String var1) throws TimeoutException, RpcCallbackException {
        String node = selector().apply(selectorNode.get(), this);
        RpcCall rpcCall = RpcCall.newInstance("RpcMethod/rpc3", var0, var1);
        rpcCall.setNode(node);
        getByName(node).call(rpcCall);
    }

    public void rpc3(Integer var0, String var1, RpcCallback<Void> callback) {
        String node = selector().apply(selectorNode.get(), this);
        RpcCall rpcCall = RpcCall.newInstance("RpcMethod/rpc3", var0, var1);
        rpcCall.setNode(node);
        getByName(node).call(rpcCall, callback);
    }

    public String retry(String var0, Integer var1) throws TimeoutException, RpcCallbackException {
        String node = selector().apply(selectorNode.get(), this);
        RpcCall rpcCall = RpcCall.newInstance("RpcMethod/retry", var0, var1);
        rpcCall.setNode(node);
        return getByName(node).call(rpcCall);
    }

    public void retry(String var0, Integer var1, RpcCallback<String> callback) {
        String node = selector().apply(selectorNode.get(), this);
        RpcCall rpcCall = RpcCall.newInstance("RpcMethod/retry", var0, var1);
        rpcCall.setNode(node);
        getByName(node).call(rpcCall, callback);
    }

    public TestObject2 rpc4(TestObject2 var0, String var1) throws TimeoutException, RpcCallbackException {
        String node = selector().apply((K) var1, this);
        RpcCall rpcCall = RpcCall.newInstance("RpcMethod/rpc4", var0, var1);
        rpcCall.setSliceKey(var1);
        rpcCall.setNode(node);
        return getByName(node).call(rpcCall);
    }

    public void rpc4(TestObject2 var0, String var1, RpcCallback<TestObject2> callback) {
        String node = selector().apply((K) var1, this);
        RpcCall rpcCall = RpcCall.newInstance("RpcMethod/rpc4", var0, var1);
        rpcCall.setSliceKey(var1);
        rpcCall.setNode(node);
        getByName(node).call(rpcCall, callback);
    }

    public TestObject2 rpc5(List<TestObject2> var0, Map<String, TestObject2> var1, String var2) throws TimeoutException, RpcCallbackException {
        String node = selector().apply(selectorNode.get(), this);
        RpcCall rpcCall = RpcCall.newInstance("RpcMethod/rpc5", var0, var1, var2);
        rpcCall.setNode(node);
        return getByName(node).call(rpcCall);
    }

    public void rpc5(List<TestObject2> var0, Map<String, TestObject2> var1, String var2, RpcCallback<TestObject2> callback) {
        String node = selector().apply(selectorNode.get(), this);
        RpcCall rpcCall = RpcCall.newInstance("RpcMethod/rpc5", var0, var1, var2);
        rpcCall.setNode(node);
        getByName(node).call(rpcCall, callback);
    }
}