package org.voovan.korla.rpc;

import org.voovan.korla.rpc.annotation.Rpc;
import org.voovan.korla.rpc.annotation.SelectorParam;
import org.voovan.tools.json.JSON;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 类文字命名
 *
 * @author: helyho
 * korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
@Rpc(value = "RpcMethod", singleton = true)
public class RpcMethodsTest {
    @Rpc
    @SelectorParam(String.class)
    public String rpc1(String aa, Integer bb) throws IOException {
        System.out.println(bb + " " + System.currentTimeMillis());
        return aa + bb;
    }

    @Rpc(forceFlush = true)
    public String rpc2(Integer aa, String bb) {
        System.out.println(aa + " " + bb + " " + System.currentTimeMillis());
        return aa  + " " + bb;
    }

    @Rpc
    public void rpc3(Integer aa, String bb) {
        System.out.println(aa + " " + bb + " " + System.currentTimeMillis());
    }

    @Rpc
    public TestObject2 rpc4(TestObject2 testObject2, @SelectorParam String node) {
        System.out.println(JSON.toJSON(testObject2) + " " + node);
        testObject2.setString(node);
        testObject2.setBint((int) System.currentTimeMillis());
        return testObject2;
    }

    @Rpc
    public TestObject2 rpc5(List<TestObject2> testObjectList, Map<String, TestObject2> testObject2Map, String aaa) {
        if(testObjectList!=null && testObjectList.size() >0) {
            TestObject2 testObject2 = testObjectList.get(0);
            System.out.println(JSON.toJSON(testObject2) + " " + aaa);
            testObject2.setBint((int) System.currentTimeMillis());
        }
        return null;
    }

    @Rpc
    public String retry(String aa, Integer bb) {
        if(System.currentTimeMillis()%5 == 0) {
            throw new RuntimeException("execption retry");
        }

        return aa + " " + bb + " " + System.currentTimeMillis();
    }

}
