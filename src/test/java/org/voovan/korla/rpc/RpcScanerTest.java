package org.voovan.korla.rpc;

/**
 * 类文字命名
 *
 * @author: helyho
 * korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
public class RpcScanerTest {
    public static void main(String[] args) {
        try {
            RpcScaner.scan("org.voovan.korla.rpc");
            System.out.println("done");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
