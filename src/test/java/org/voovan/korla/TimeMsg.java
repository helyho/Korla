package org.voovan.korla;

import org.voovan.korla.message.Msg;

/**
 * 类文字命名
 *
 * @author: helyho
 * korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
public class TimeMsg extends Msg {
    private long time;
    private int seq;

    public TimeMsg() {
        super();
        this.time = System.currentTimeMillis();
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long diff(){
        return System.currentTimeMillis() - time;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String toString() {
        return "id:"+ getId() + ", time:" + time + ", diffTime:" + diff() + ", seq:" +getSeq();
    }
}
