package org.voovan.korla.exception;

/**
 * Korla 异常
 *
 * @author: helyho
 * korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
public class KorlaException extends RuntimeException {
    public KorlaException(String msg){
        super(msg);
    }

    public KorlaException(String msg, Exception e){
        super(msg, e);
    }
}
