package org.voovan.korla.rpc.annotation;

import java.lang.annotation.*;

/**
 * Rpc 服务注解
 *
 * @author: helyho
 * korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface Rpc {
    String value() default "";

    /**
     * Provicer 幂等性
     *  默认:true
     * @return true: 开启框架会自动进行幂等数据保存和幂等响应
     */
    boolean idempotence() default false;

    /**
     * 定义 RPC 否采用单例模式
     * 在类上则标识类会被提前实例化, 在RPC方法上,则使用提前实例化的类进行调用
     * 在方法上无效
     * @return true: 单例模式, false: 非单例模式
     */
    boolean singleton() default true;

    /**
     * 是否强制 flush
     * @return true: 强制 flush, false: 自动 flush
     */
    boolean forceFlush() default false;
}
