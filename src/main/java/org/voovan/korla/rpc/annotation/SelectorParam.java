package org.voovan.korla.rpc.annotation;

import java.lang.annotation.*;

/**
 * 用于计算集群服务节点用的参数
 *
 * @author: helyho
 * Korla Framework.
 * WebSite: https://github.com/helyho/Korla
 * Licence: Apache v2 License
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Documented
public @interface SelectorParam {
    Class value() default Object.class;
}
