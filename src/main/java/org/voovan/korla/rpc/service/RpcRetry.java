package org.voovan.korla.rpc.service;

/**
 * 重试判断
 *
 * @author: helyho
 * korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
public interface RpcRetry {

    default boolean canRetry(int retryCount, Exception e) {
         return false;
    }
}
