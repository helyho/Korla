package org.voovan.korla.rpc.service.selector;

import org.voovan.korla.rpc.service.RpcService;

import java.util.Map;
import java.util.function.BiFunction;

/**
 * Rpc 服务选择器随机选择器
 *
 * @author: helyho
 * Korla Framework.
 * WebSite: https://github.com/helyho/Korla
 * Licence: Apache v2 License
 */
public class RandomSelector<K> implements BiFunction<K, RpcService, String> {

    @Override
    public String apply(Object o, RpcService rpcService) {
        Map container = rpcService.getAllConsumer();
        int index = (int) (container.size() * Math.random());

        String node = (String)container.keySet().toArray()[index];
        return node;
    }
}
