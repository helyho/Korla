package org.voovan.korla.rpc.service.selector;

import org.voovan.korla.rpc.service.RpcService;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;

/**
 * Rpc 服务选择器, 根据名称选择服务
 *
 * @author: helyho
 * Korla Framework.
 * WebSite: https://github.com/helyho/Korla
 * Licence: Apache v2 License
 */
public class NameSelector<K> implements BiFunction<K, RpcService, String> {
    private AtomicInteger currentIndex = new AtomicInteger(0);

    @Override
    public String apply(Object o, RpcService rpcService) {

        return o.toString();
    }
}
