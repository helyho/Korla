package org.voovan.korla.rpc.service.selector;

import org.voovan.korla.rpc.service.RpcConsumer;
import org.voovan.korla.rpc.service.RpcService;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;

/**
 * Rpc 服务选择器轮训选择器
 *
 * @author: helyho
 * Korla Framework.
 * WebSite: https://github.com/helyho/Korla
 * Licence: Apache v2 License
 */
public class LoopSelector<K> implements BiFunction<K, RpcService, String> {
    private AtomicInteger currentIndex = new AtomicInteger(0);

    @Override
    public String apply(Object o, RpcService rpcService) {
        Map container = rpcService.getAllConsumer();
        int index = currentIndex.getAndIncrement();
        index = index%container.size();

        String node = (String)container.keySet().toArray()[index];
        return node;
    }
}
