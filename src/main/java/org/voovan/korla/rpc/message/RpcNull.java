package org.voovan.korla.rpc.message;

import java.util.List;

/**
 * Rpc 方法参数中 null 的占位对象
 *
 * @author: helyho
 * Korla Framework.
 * WebSite: https://github.com/helyho/Korla
 * Licence: Apache v2 License
 */
public class RpcNull {
    public final static RpcNull RPC_NULL_ARGS = new RpcNull();

    @Override
    public boolean equals(Object obj) {
        return obj instanceof RpcNull;
    }
}
