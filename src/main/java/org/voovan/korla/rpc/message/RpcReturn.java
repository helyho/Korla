package org.voovan.korla.rpc.message;

import org.voovan.korla.message.Msg;
import org.voovan.tools.TObject;
import org.voovan.tools.serialize.TSerialize;

/**
 * Rpc 响应对象
 *
 * @author: helyho
 * korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
public class RpcReturn<T> extends Msg {
    public final static int RPC_RETURN_VALUE = 0;
    public final static int RPC_RETURN_EXCEPTION = 1;

    private int returnType;
    private byte[] returnValue;
    private String[] exception;

    public RpcReturn() {

    }

    public int getReturnType() {
        return returnType;
    }

    public void setReturnType(int returnType) {
        this.returnType = returnType;
    }

    public T getReturnValue() {
        T ret =  (T) TSerialize.unserialize(returnValue);
        return ret.equals(RpcNull.RPC_NULL_ARGS) ? null : ret;
    }

    public void setReturnValue(T returnValue) {
        this.returnValue = TSerialize.serialize(returnValue==null ? RpcNull.RPC_NULL_ARGS : returnValue);
    }

    public String[] getException() {
        return exception;
    }

    public void setException(String methoNdame, String message) {
        this.exception = this.exception == null ? new String[2] : this.exception;
        this.exception[0] = methoNdame;
        this.exception[1] = TObject.nullDefault(message, "null");
    }
}
