package org.voovan.korla.rpc.message;

import org.voovan.korla.rpc.exception.RpcCallbackException;
import org.voovan.tools.log.Logger;

import java.util.concurrent.TimeoutException;

/**
 * Rpc 服务回调
 *
 * @author: helyho
 * korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
public interface RpcCallback<T> {
    default RpcCall prepare(RpcCall rpcCall) {
        return rpcCall;
    }
    public void onReturn(T returnValue);
    public void onException(RpcCallbackException e);
    default void onTimeout(TimeoutException e) {
        Logger.error("[Korla] RpcCallback has error", e);
    }
}
