package org.voovan.korla.rpc;

import org.voovan.korla.rpc.message.RpcCall;
import org.voovan.korla.rpc.message.RpcNull;
import org.voovan.korla.rpc.message.RpcReturn;
import org.voovan.tools.TObject;
import org.voovan.tools.TProperties;
import org.voovan.tools.log.Logger;
import org.voovan.tools.serialize.TSerialize;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Rpc 消息静态类
 *
 * @author: helyho
 * korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
public class RpcStatic {
    public static String CONSUMER_CODE_PACKAGE = "org.voovan.korla.rpc.consumer";

    public static ConcurrentHashMap<Class, Object>      RPC_SINGLETON_OBJECTS = new ConcurrentHashMap<Class, Object>();
    public static ConcurrentHashMap<String, RpcMethod>  RPC_METHODS           = new ConcurrentHashMap<String, RpcMethod>();

    static {
        TSerialize.register(RpcNull.class);

        try {
            RpcMethod rpcMethod = new RpcMethod("RpcCommon", "getAllRpcMethod", true, true, true, RpcStatic.class.getMethod("getAllRpcMethod"));
            RPC_METHODS.putIfAbsent(rpcMethod.getName(), rpcMethod);
        } catch (NoSuchMethodException e) {
            Logger.error("[Korla] RpcStatic.init has error", e);
        }
    }

    public  static String RPC_SCAN_PACKAGE = "";
    public  static Integer CALL_TIMEOUT = TProperties.getInt("korla","korla.rpc.callTimeout", 10)*1000;

    //序列化类型注册
    public static void registerClass() {
        TSerialize.register(RpcCall.class);
        TSerialize.register(RpcReturn.class);
        TSerialize.register(RpcNull.class);
    }

    public static Collection<RpcMethod> getAllRpcMethod() {
        return new ArrayList<RpcMethod>(RPC_METHODS.values());
    }
}
