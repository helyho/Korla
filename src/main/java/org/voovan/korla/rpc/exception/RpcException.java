package org.voovan.korla.rpc.exception;

/**
 * Rpc 异常
 *
 * @author: helyho
 * korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
public class RpcException extends RuntimeException {
    public RpcException(String msg){
        super(msg);
    }

    public RpcException(String msg, Exception e) {
        super(msg, e);
    }
}
