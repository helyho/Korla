package org.voovan.korla.rpc.exception;

/**
 * Rpc 异常
 *
 * @author: helyho
 * korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
public class RpcCallbackException extends Exception {
    String originMsg;
    public RpcCallbackException(String msg, String originMsg){
        super(msg);
        this.originMsg = originMsg;
    }

    public RpcCallbackException(String msg, String originMsg,  Exception e) {
        super(msg, e);
        this.originMsg = originMsg;
    }

    public String getOriginMsg() {
        return originMsg;
    }
}
