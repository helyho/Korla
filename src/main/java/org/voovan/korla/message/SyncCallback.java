package org.voovan.korla.message;

/**
 * 同步回调器
 *
 * @author: helyho
 * korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
public class SyncCallback extends Callback {

    private transient Msg reciveMsg;

    public SyncCallback(Msg reciveMsg) {
        this.reciveMsg = reciveMsg;
    }

    @Override
    public Msg apply(Msg msg) {
        reciveMsg.setRespons(msg);
        return null;
    }
}
