package org.voovan.korla.message;

/**
 * 回调抽象类
 *
 * @author: helyho
 * korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
public abstract class Callback<C extends Msg, R extends Msg> {
    public abstract R apply(C t);

    public R execute(C msg) {
        R result = apply(msg);
        msg.setCallBack(null);
        msg.setDone(true);

        if(result!=null) {
            result.setDone(result.getCallBack() == null);
        }

        return result;
    }
}
