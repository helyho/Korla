package org.voovan.korla.message;


import static org.voovan.korla.KorlaStatic.DEFAULT_CHANNEL;

/**
 * Korla 注册消息
 *
 * @author: helyho
 * korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
public class MsgRegister extends Msg {

    public MsgRegister() {
        this(DEFAULT_CHANNEL);
    }

    public MsgRegister(String channel) {
        this.setChannel(channel);
        this.setType(MSG_TYPE_REG);
    }
}
