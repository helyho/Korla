package org.voovan.korla.handler;

import org.voovan.korla.message.Msg;
import org.voovan.network.IoSession;

/**
 * 默认业务句柄
 *
 * @author: helyho
 * korla Framework.
 * WebSite: https://github.com/helyho/korla
 * Licence: Apache v2 License
 */
public interface Handler {
    public Msg connect(IoSession session);
    public void disconnect(IoSession session);
    public Msg receive(IoSession session, Msg msg);
    public void exception(IoSession session, Exception e);
}
